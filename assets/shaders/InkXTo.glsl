#version 150
precision highp float;
uniform sampler2D FixInkMap;
uniform sampler2D SinkInkMap;
uniform sampler2D velDen;
uniform float bEvaporToDisapper; // false = 0, true = 1

in VSOUT
{
    vec2 texCoord;
} fsin;

out vec4 fragColor;

void main(void)
{
    vec2 Tex0 = fsin.texCoord;
    
    vec4 sink = texture(SinkInkMap, Tex0);
    vec4 ix0 = texture(FixInkMap, Tex0);
    vec4 ix_new = sink + ix0;
    ix_new = vec4(ix_new.xyz, (ix_new.x + ix_new.y + ix_new.z) / 3.0);
    
    if (bEvaporToDisapper == 1.0)
    {
        vec4 a = texture(velDen, Tex0);
        if (a.z <= 0.001)
            fragColor = vec4(ix_new.rgb * (a.z * 990.0), ix_new.a);
        else
            fragColor = ix_new;
    }
    else
    {
        fragColor = ix_new;
    }
}