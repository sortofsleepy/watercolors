#version 150
precision highp float;
uniform mat4 ciProjectionMatrix;
uniform mat4 ciModelView;
uniform mat4 ciModelViewProjection;

uniform vec2 pxSize;

in vec4  ciPosition;
in vec2  ciTexCoord0;
in vec4  ciColor;
in vec3  ciNormal;


out VSOUT
{
    vec2 texCoord;
} vsout;


void main()
{
    
    vsout.texCoord = ciTexCoord0;
    gl_Position = ciModelViewProjection * ciPosition;
}
