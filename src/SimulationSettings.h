//
//  SimulationSettings.h
//  WaterColor
//
//  Created by Joseph Chow on 1/3/16.
//
//

#ifndef WaterColor_SimulationSettings_h
#define WaterColor_SimulationSettings_h

struct SimulationSettings {
    float brushSize = 7.086956501;
    float baseMask = 0.037267081;
    float gamma = 0.037267081;
    float omega = 0.968944073;
    float advect_p = 0.100000001;
    float evapor_b = 0.000010000;
    float evapor = 0.00500000;
    float b11 = 0.009316770;
    float b12 = 0.391304344;
    float b13 = 0.009316770;
    float b21 = 0.123152710;
    float b22 = 0.307453424;
    float p1 = 0.000000000;
    float p2 = 0.300000012;
    float p3 = 0.200000003;
    float ba1 = 0.000040994;
    float ba2 = 0.000043168;
    float f1 = 0.010000000;
    float f2 = 0.090000004;
    float f3 = 0.090000004;
    float toe_p = 0.100000001;
    float waterAmount = 1.000000000;
    float wf_mul = 1.0f;
    float width = cinder::app::getWindowWidth();
    float height = cinder::app::getWindowHeight();
};

#endif
