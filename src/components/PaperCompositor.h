//
//  PaperCompositor.h
//  Colors
//  Composites the textures to form the paper base for the coloring
//  Created by Joseph Chow on 1/3/16.
//
//

#ifndef Colors_PaperCompositor_h
#define Colors_PaperCompositor_h

using namespace ci;
using namespace std;
using namespace ci::app;

#define STRINGIFY(A) #A

struct PaperCompositor {


    ci::gl::GlslProgRef shader;
    ci::gl::FboRef buffer;

    float width,height;
    PaperCompositor ()= default;

    PaperCompositor(int width,int height){
  
        buffer = gl::Fbo::create(width, height);
        
        gl::GlslProg::Format fmt;
        fmt.vertex(vertex);
        fmt.fragment(fragment);
        fmt.version(150);
        shader = gl::GlslProg::create(fmt);
        
        this->width = width;
        this->height = height;
        
        gl::ScopedFramebuffer fbo(buffer);{
            gl::clear(ColorA(0,0,0,0));
        }
        
    }
    
    string vertex = STRINGIFY(
                    
                              precision highp float;
                              uniform mat4 ciProjectionMatrix;
                              uniform mat4 ciModelView;
                              uniform mat4 ciModelViewProjection;
                              
                              uniform vec2 pxSize;
                              
                              in vec4  ciPosition;
                              in vec2  ciTexCoord0;
                              in vec4  ciColor;
                              in vec3  ciNormal;
                              
                              
                              out vec2 texCoord;
                              
                              void main()
                            {
        
                                texCoord = ciTexCoord0;
                                gl_Position = ciModelViewProjection * ciPosition;
                            }
                              );
    
    string fragment = STRINGIFY(
                                uniform sampler2D Grain;
                                uniform sampler2D Alum;
                                uniform sampler2D Pinning;
                                
                                in vec2 texCoord;
                                
                                out vec4 fragColor;
                                
                                void main(void)
                                {
                                    vec2 Tex0 = texCoord;
                                    
                                    vec4 g = texture(Grain, Tex0);
                                    vec4 a = texture(Alum, Tex0);
                                    vec4 p = texture(Pinning, Tex0);
                                    
                                    float gg = (g.x + g.y + g.z) / 3.0;
                                    float aa = (a.x + a.y + a.z) / 3.0;
                                    float pp = (p.x + p.y + p.z) / 3.0;
                                    float co = 1.0;
                                    
                                    gg *= co;
                                    aa *= co;
                                    pp *= co;
                                    
                                    fragColor = vec4(gg, 1.0, aa, pp);
                                });
};

#endif
