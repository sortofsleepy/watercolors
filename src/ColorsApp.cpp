#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "Paint.h"
#include "PingPongBuffer.h"
using namespace ci;
using namespace ci::app;
using namespace std;

#define SIZE 512

class ColorsApp : public App {
public:
    void setup() override;
    void mouseDown( MouseEvent event ) override;
    void update() override;
    void draw() override;
    Paint sim;
    
};

void ColorsApp::setup()
{
    sim.setupPaper("grain.jpg", "alum.jpg", "pinning.jpg");
}

void ColorsApp::mouseDown( MouseEvent event )
{
    sim.begin();
    gl::drawSolidCircle(event.getPos(), 20);
    sim.end();
    
}

void ColorsApp::update()
{
    sim.update();
}

void ColorsApp::draw()
{
    gl::clear( Color( 0, 0, 0 ) );
    sim.draw();
    
}

CINDER_APP( ColorsApp, RendererGl )
