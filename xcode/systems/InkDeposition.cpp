//
//  InkDeposition.cpp
//  Colors
//
//  Created by Joseph Chow on 1/6/16.
//
//

#include "InkDeposition.h"
using namespace ci;
using namespace std;
using namespace ci::app;
using namespace entityx;

InkDeposition::InkDeposition(SimulationSettings * settings){
    this->settings = settings;
}
void InkDeposition::update(entityx::EntityManager &entities, entityx::EventManager &events, entityx::TimeDelta dt){
    ComponentHandle<InputBuffer> input;
    ComponentHandle<InkBuffer> misc;
    ComponentHandle<SurfaceInk> surfInk;
    for( auto __unused e : entities.entities_with_components( misc, surfInk, input ) )
    {
        surfInk->begin();
        addpigment.update(settings->width,
                          settings->height,
                          settings->gamma,
                          settings->baseMask,
                          input->getInput(),
                          misc->getOutput());
        surfInk->end();
        
        misc->begin();
        addwater.update(settings->width,
                        settings->height,
                        settings->gamma,
                        settings->baseMask,
                        settings->waterAmount,
                        input->getInput());
        misc->end();
    
    }

}