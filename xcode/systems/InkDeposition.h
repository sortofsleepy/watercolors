//
//  InkDeposition.h
//  Colors
//  Responsible for depositing the "ink" onto the "paper"
//  Created by Joseph Chow on 1/6/16.
//
//

#ifndef __Colors__InkDeposition__
#define __Colors__InkDeposition__

#include "entityx/System.h"
#include "SurfaceInk.h"
#include "InkBuffer.h"
#include "AddPigment.h"
#include "AddWater.h"
#include "SimulationSettings.h"
#include "InputBuffer.h"

class InkDeposition : public entityx::System<InkDeposition> {
    
public:
    InkDeposition(SimulationSettings * settings);
    void update( entityx::EntityManager &entities, entityx::EventManager &events, entityx::TimeDelta dt ) override;
private:
    entityx::TimeDelta  previous_dt = 1.0 / 60.0;
    
    SimulationSettings * settings;
    AddWaterShader addwater;
    AddPigmentShader addpigment;
};

#endif /* defined(__Colors__InkDeposition__) */
