//
//  PaperConstruction.cpp
//  Colors
//   A system responsible for construcing the paper base.
//  Created by Joseph Chow on 1/5/16.
//
//

#include "PaperConstruction.h"
using namespace ci;
using namespace entityx;
using namespace ci::app;

void PaperConstruction::update( EntityManager &entities, EventManager &events, TimeDelta dt )
{
    ComponentHandle<PaperTextures> textures;
    ComponentHandle<PaperCompositor> compositor;
    ComponentHandle<Watercolor> base;
    ComponentHandle<BlendComponent> blending;
    for( auto __unused e : entities.entities_with_components( textures,compositor,base,blending) )
    {
        
     
        auto paper = *textures.get();
        auto composit = *compositor.get();
        auto box = *base.get();
        
        blending->enableBlend();
        gl::ScopedFramebuffer fbo(composit.buffer);{
            gl::setMatricesWindow(composit.width, composit.height);
            gl::viewport(0,0,composit.width,composit.height);
            gl::ScopedGlslProg shd(composit.shader);{
                gl::ScopedTextureBind tex0(paper.t1,0);
                gl::ScopedTextureBind tex1(paper.t2,1);
                gl::ScopedTextureBind tex2(paper.t3,2);
                composit.shader->uniform("Grain",0);
                composit.shader->uniform(("Alum"), 1);
                composit.shader->uniform("Pinning",2);
                gl::drawSolidRect(Rectf(0,0,composit.width,composit.height));
            }
            
        }
        blending->disableBlend();
        
        
        base->paper = composit.buffer->getColorTexture();
        
    }
}
