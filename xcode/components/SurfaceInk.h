//
//  SurfaceInk.h
//  Colors
//
//  Created by Joseph Chow on 1/6/16.
//
//

#ifndef Colors_SurfaceInk_h
#define Colors_SurfaceInk_h

#include "PingPongBuffer.h"
using namespace ci;
using namespace ci::app;
using namespace std;

struct SurfaceInk {

    SurfaceInk(){
        surfaceInk.allocate();
    }
    
    gl::TextureRef getOutput(){
        return surfaceInk.getOutput();
    }
    
    void begin(){
        surfaceInk.begin();
    }
    
    void end(){
        surfaceInk.end();
    }
    PingPongBuffer surfaceInk;
    
};

#endif
