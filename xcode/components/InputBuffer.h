//
//  InputBuffer.h
//  Colors
//  A component repsonsible for taking in user input
//  Created by Joseph Chow on 1/6/16.
//
//

#ifndef Colors_InputBuffer_h
#define Colors_InputBuffer_h

using namespace ci;
using namespace ci::app;
using namespace std;

struct InputBuffer{
    InputBuffer(float width=getWindowWidth(),float height=getWindowHeight(),gl::Fbo::Format settings = gl::Fbo::Format()){
        fbo = gl::Fbo::create(width, height,settings);
        
        gl::ScopedFramebuffer buff(fbo);{
            gl::clear(ColorA(0,0,0,0));
        }
    }
    void begin(){
        fbo->bindFramebuffer();
        gl::clear(ColorA(0,0,0,0));
    }
    
    void end(){
        fbo->unbindFramebuffer();
    }
    
    gl::TextureRef getInput(){
        return fbo->getColorTexture();
    }
    
    ci::gl::FboRef fbo;
};

#endif
