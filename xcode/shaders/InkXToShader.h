//
//  InkXToShader.h
//  WaterColors
//
//  Created by Joseph Chow on 12/30/15.
//
//

#ifndef WaterColors_InkXToShader_h
#define WaterColors_InkXToShader_h
#include "InkShader.h"

class InkXToShader: public InkShader {
public:
    InkXToShader(){
        loadShader("InkXTo.glsl","shaders");
        
    }
    void update(int w, int h,
                vec2 pxSize,
                //const gl::TextureRef& fixInk,
                const gl::TextureRef& sinkInk,
                const gl::TextureRef& velDen,
                float bEvaporToDisapper)
    {
        gl::ScopedGlslProg shd(shader);
        //gl::ScopedTextureBind tex0(fixInk,0);
        gl::ScopedTextureBind tex1(sinkInk,1);
        gl::ScopedTextureBind tex2(velDen,2);
        
        //shader->uniform("pxSize", pxSize);
        //shader->uniform("FixInkMap", 0);
        shader->uniform("SinkInkMap", 1);
        shader->uniform("velDen", 2);
        shader->uniform("bEvaporToDisapper", bEvaporToDisapper);
        drawPlane(w, h);
       
    }
    
};

#endif
