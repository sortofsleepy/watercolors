//
//  InkSupply.h
//  WaterColors
//
//  Created by Joseph Chow on 12/29/15.
//
//

#ifndef WaterColors_InkSupply_h
#define WaterColors_InkSupply_h
#include "InkShader.h"

class InkSupplyShader : public InkShader {
public:
    InkSupplyShader(){
        loadShader("InkSupply.glsl","shaders");
    }
    
    
    void update(int w, int h,
                vec2 pxSize,
                const gl::TextureRef& velDen,
                //const gl::TextureRef& surfInk,
                const gl::TextureRef& misc)
    {
        gl::ScopedGlslProg shd(shader);
        gl::ScopedTextureBind tex0(velDen,1);
       // gl::ScopedTextureBind tex1(surfInk,1);
        gl::ScopedTextureBind tex2(misc,2);
        
        shader->uniform("pxSize", pxSize);
        shader->uniform("VelDenMap",  1);
        shader->uniform("SurfInkMap", 0);
        shader->uniform("MiscMap",  2);
        drawPlane(w, h);
        
    }
    
};


#endif
