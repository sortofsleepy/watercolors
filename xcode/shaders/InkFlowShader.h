//
//  InkFlowShader.h
//  WaterColors
//
//  Created by Joseph Chow on 12/30/15.
//
//

#ifndef WaterColors_InkFlowShader_h
#define WaterColors_InkFlowShader_h
#include "InkShader.h"

class InkFlowShader : public InkShader {
public:
    InkFlowShader(){
        loadShader("InkFlow.glsl","shaders");
    }
    
    void update(int w, int h,
                vec2 pxSize,
                float ba1, float ba2,
                vec2 offset,
                const gl::TextureRef& velDen,
                const gl::TextureRef& misc,
                const gl::TextureRef& dist1,
                const gl::TextureRef& dist2,
                //const gl::TextureRef& flowInk,
                const gl::TextureRef& surfInk)
    {
        gl::ScopedGlslProg shd(shader);
        gl::ScopedTextureBind tex0(velDen,1);
        gl::ScopedTextureBind tex1(misc,2);
        gl::ScopedTextureBind tex2(dist1,3);
        gl::ScopedTextureBind tex3(dist2,4);
//        gl::ScopedTextureBind tex4(flowInk,0);
        gl::ScopedTextureBind tex5(surfInk,5);
        
        //shader->uniform("pxSize", pxSize);
        shader->uniform("Blk_a", vec2(ba1, ba2));
        shader->uniform("offset", offset);
        shader->uniform("VelDenMap", 1);
        shader->uniform("MiscMap",  2);
        shader->uniform("Dist1Map", 3);
        shader->uniform("Dist2Map",  4);
        shader->uniform("FlowInkMap",  0);
        shader->uniform("SurfInkMap",  5);
        drawPlane(w, h);
   
    }
    
};



#endif
