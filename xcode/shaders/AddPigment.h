//
//  AddPigment.h
//  WaterColors
//
//  Created by Joseph Chow on 12/29/15.
//
//

#ifndef WaterColors_AddPigment_h
#define WaterColors_AddPigment_h

#include "InkShader.h"

class AddPigmentShader : public InkShader{
public:
    AddPigmentShader(){
        loadShader("AddPigment.glsl","shaders");
    }
    
    void update(int w, int h,
               // vec2 pxSize,
                float gamma,
                float baseMask,
                gl::TextureRef depositionBuffer,
                gl::TextureRef misc)
    {
        gl::ScopedGlslProg shd(shader);
     
        gl::ScopedTextureBind tex1(depositionBuffer,1);
        gl::ScopedTextureBind tex2(misc,2);
        
        //shader->uniform("pxSize", pxSize);
        shader->uniform("gamma", gamma);
        shader->uniform("baseMask", baseMask);
        shader->uniform("SurfInk", 0);
        shader->uniform("WaterSurface",1);
        shader->uniform("Misc",  2);
        drawPlane(w, h);
    }
    
};
#endif
