//
//  Collide1Shader.h
//  WaterColors
//
//  Created by Joseph Chow on 12/29/15.
//
//

#ifndef WaterColors_Collide1Shader_h
#define WaterColors_Collide1Shader_h
#include "InkShader.h"

class Collide1Shader : public InkShader{
public:
    Collide1Shader(){
        loadShader("Collide1.glsl","shaders");
    }
    
    void update(int w, int h,
                vec2 pxSize,
                float advect_p,
                float omega,
                const gl::TextureRef& velDen,
                //const gl::TextureRef& dist1,
                const gl::TextureRef& flowInk)
    {
        gl::ScopedGlslProg shd(shader);
        gl::ScopedTextureBind tex0(velDen,1);
       // gl::ScopedTextureBind tex1(dist1,1);
        gl::ScopedTextureBind tex2(flowInk,2);
        
        shader->uniform("pxSize", pxSize);
        shader->uniform("A", 0.1111111f);
        shader->uniform("B", 0.3333333f);
        shader->uniform("C", 0.5f);
        shader->uniform("D", 0.1666667f);
        shader->uniform("advect_p", advect_p);
        shader->uniform("Omega", omega);
        
        shader->uniform("VelDenMap",  1);
        shader->uniform("Dist1Map", 0);
        shader->uniform("InkMap", 2);
        drawPlane(w, h);
        
    }
};


#endif
