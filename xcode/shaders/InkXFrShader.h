//
//  InkXFrShader.h
//  WaterColors
//
//  Created by Joseph Chow on 12/30/15.
//
//

#ifndef WaterColors_InkXFrShader_h
#define WaterColors_InkXFrShader_h

#include "InkShader.h"

class InkXFrShader : public InkShader {
public:
    InkXFrShader(){
        loadShader("InkXFr.glsl","shaders");
        
    }
    void update(int w, int h,
                vec2 pxSize,
                //const gl::TextureRef& flowInk,
                const gl::TextureRef& sinkInk)
    {
        gl::ScopedGlslProg shd(shader);
        //gl::ScopedTextureBind tex0(flowInk,0);
        gl::ScopedTextureBind tex1(sinkInk,1);
        
        //shader->uniform("pxSize", pxSize);
        shader->uniform("FlowInkMap", 0);
        shader->uniform("SinkInkMap", 1);
        drawPlane(w, h);
        
    }
};
#endif
