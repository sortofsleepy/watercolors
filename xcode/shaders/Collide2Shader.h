//
//  Collide2Shader.h
//  WaterColors
//
//  Created by Joseph Chow on 12/29/15.
//
//

#ifndef WaterColors_Collide2Shader_h
#define WaterColors_Collide2Shader_h

#include "InkShader.h"

class Collide2Shader : public InkShader{
public:
    Collide2Shader(){
        loadShader("Collide2.glsl","shaders");
    }
    
    void update(int w, int h,
                vec2 pxSize,
                float advect_p,
                float omega,
                const gl::TextureRef& velDen,
                //const gl::TextureRef& dist2,
                const gl::TextureRef& flowInk)
    {
        gl::ScopedGlslProg shd(shader);
        gl::ScopedTextureBind tex0(velDen,1);
        //gl::ScopedTextureBind tex1(dist2,1);
        gl::ScopedTextureBind tex2(flowInk,2);
        
        shader->uniform("pxSize", pxSize);
        shader->uniform("A", 0.02777778f);
        shader->uniform("B", 0.08333334f);
        shader->uniform("C", 0.125f);
        shader->uniform("D", 0.04166667f);
        shader->uniform("advect_p", advect_p);
        shader->uniform("Omega", omega);
        shader->uniform("VelDenMap", 1);
        shader->uniform("Dist2Map", 0);
        shader->uniform("InkMap",2);
        drawPlane(w, h);
        
    }
    
};



#endif
