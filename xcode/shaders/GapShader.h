//
//  GapShader.h
//  WaterColors
//
//  Created by Joseph Chow on 12/26/15.
//
//

#ifndef WaterColors_GapShader_h
#define WaterColors_GapShader_h

#include "InkShader.h"

class GapShader : public InkShader {
public:
    GapShader(){
        loadShader("GapShader.glsl","shaders");
    }
    
    void update(int w, int h,
                vec2 pxSize,
                gl::TextureRef grain,
                gl::TextureRef alum,
                gl::TextureRef pinning)
    {
        gl::ScopedGlslProg shd(shader);{
            gl::ScopedTextureBind tex0(grain,0);
            gl::ScopedTextureBind tex1(alum,1);
            gl::ScopedTextureBind tex2(pinning,2);
            
            //shader->uniform("pxSize", pxSize);
            
            shader->uniform("Grain", 0);
            shader->uniform("Alum",1);
            shader->uniform("Pinning", 2);
            drawPlane(w,h);
      
        }
        
    }
    
};
#endif
