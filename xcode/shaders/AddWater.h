//
//  AddWater.h
//  WaterColors
//
//  Created by Joseph Chow on 12/29/15.
//
//

#ifndef WaterColors_AddWater_h
#define WaterColors_AddWater_h

#include "InkShader.h"

class AddWaterShader : public InkShader{
public:
    AddWaterShader(){
        loadShader("AddWater.glsl","shaders");
    }
    
    void update(int w, int h,
                //vec2 pxSize,
                float gamma,
                float baseMask,
                float waterAmount,
                //gl::TextureRef misc,
                gl::TextureRef depositionBuffer)
    {
        gl::ScopedGlslProg shd(shader);
        //gl::ScopedTextureBind tex1(misc,0);
        gl::ScopedTextureBind tex2(depositionBuffer,1);
        
        //shader->uniform("pxSize", pxSize);
        shader->uniform("gamma", gamma);
        shader->uniform("baseMask", baseMask);
        shader->uniform("waterAmount",waterAmount);
        
        shader->uniform("Misc",  0);
        shader->uniform("WaterSurface",1);
        drawPlane(w, h);
    }
    
};

#endif
