//
//  Stream2Shader.h
//  WaterColors
//
//  Created by Joseph Chow on 12/29/15.
//
//

#ifndef WaterColors_Stream2Shader_h
#define WaterColors_Stream2Shader_h


#include "InkShader.h"

class Stream2Shader : public InkShader{
public:
    Stream2Shader(){
        loadShader("Stream2.glsl","shaders");
    }
    
    void update(int w, int h,
                vec2 pxSize,
                float evapor_b,
                vec2 offset,
                const gl::TextureRef& misc,
                const gl::TextureRef& dist2)
    {
        gl::ScopedGlslProg shd(shader);
        gl::ScopedTextureBind tex0(misc,0);
        gl::ScopedTextureBind tex1(dist2,1);
        
        shader->uniform("pxSize", pxSize);
        shader->uniform("Evapor_b", evapor_b);
        shader->uniform("offset", offset);
        shader->uniform("MiscMap", 0);
        shader->uniform("Dist2Map", 1);
        drawPlane(w, h);
        
    }
};

#endif
