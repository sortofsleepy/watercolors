//
//  Stream1Shader.h
//  WaterColors
//
//  Created by Joseph Chow on 12/29/15.
//
//

#ifndef WaterColors_Stream1Shader_h
#define WaterColors_Stream1Shader_h


#include "InkShader.h"

class Stream1Shader : public InkShader{
public:
    Stream1Shader(){
        loadShader("Stream1.glsl","shaders");
    }
    
    void update(int w, int h,
                vec2 pxSize,
                float evapor_b,
                vec2 offset,
                const gl::TextureRef& misc
                //const gl::TextureRef& dist1
                )
    {
        gl::ScopedGlslProg shd(shader);
        gl::ScopedTextureBind tex0(misc,1);
        //gl::ScopedTextureBind tex1(dist1,1);
        
        shader->uniform("pxSize", pxSize);
        shader->uniform("Evapor_b", evapor_b);
        shader->uniform("offset", offset);
        shader->uniform("MiscMap", 1);
        shader->uniform("Dist1Map",0);
        drawPlane(w, h);
        
    }
};


#endif
