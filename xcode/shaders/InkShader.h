//
//  InkShader.h
//  WaterColors
//
//  Created by Joseph Chow on 12/26/15.
//
//

#ifndef WaterColors_InkShader_h
#define WaterColors_InkShader_h


using namespace std;
using namespace ci;
using namespace ci::app;
class InkShader {
protected:
    ci::gl::VertBatchRef quad;
    ci::gl::GlslProgRef shader;
    ci::gl::VboMeshRef batch;
    
    
public:
    InkShader(){
        quad = gl::VertBatch::create();
    }
    
    void drawPlane(int width,int height){
        quad->clear();
        
        quad->begin(GL_TRIANGLE_FAN);
        quad->vertex(vec3(0));
        quad->vertex(vec3(width,0,0));
        quad->vertex(vec3(width, height, 0));
        quad->vertex(vec3(0, height, 0));
        
        quad->texCoord(vec2(0, 0));
        quad->texCoord(vec2(1,0));
        quad->texCoord(vec2(1,1));
        quad->texCoord(vec2(0,1));

        quad->end();
        //quad->draw();
        gl::drawSolidRect(Rectf(0,0,width,height));
    }
    
    void loadShader(string fragPath,string baseDir=""){
      
        gl::GlslProg::Format fmt;
        fmt.vertex(loadAsset(baseDir + "/vertex.glsl"));
        fmt.fragment(loadAsset(baseDir + "/" + fragPath));
        fmt.version(150);
    
        shader = gl::GlslProg::create(fmt);
    }
    
    
};
#endif
