//
//  Paint.h
//
//  A port of Akira Hayasaka's ofxPaint to Cinder
//  https://github.com/Akira-Hayasaka/ofxPaint
//
//  Created by Joseph Chow on 1/3/16.
//

#ifndef __WaterColor__Paint__
#define __WaterColor__Paint__

#include "entityx/EntityX.h"

//all of the necessary components to build the paper
#include "PaperTextures.h"
#include "PaperCompositor.h"
#include "PaperConstruction.h"
#include "BlendComponent.h"

#include "InkDeposition.h"
#include "Watercolor.h"

#include "SurfaceInk.h"
#include "InkBuffer.h"
#include "InputBuffer.h"

#include "PingPongBuffer.h"

#include "GetVelDenShader.h"
#include "BlockShader.h"
#include "GapShader.h"
#include "AddPigment.h"
#include "AddWater.h"
#include "SimulationSettings.h"
#include "Collide1Shader.h"
#include "Collide2Shader.h"
#include "Stream1Shader.h"
#include "Stream2Shader.h"
#include "InkSupply.h"
#include "InkXAmtShader.h"
#include "InkXToShader.h"
#include "InkXFrShader.h"
#include "InkFlowShader.h"
#include "PingPongBuffer.h"


class Paint
{
    
    
    entityx::EventManager  events;
    entityx::EntityManager entities;
    entityx::SystemManager systems;
    
    //! entity representing the whole system
    entityx::Entity paint;
    
    //! a reference to the current situation of the simulation
    entityx::ComponentHandle<Watercolor> simulation;
    
    //! reference to the input buffer
    entityx::ComponentHandle<InputBuffer> input;
    
    //! refrence to the misc buffer in the original plugin
    entityx::ComponentHandle<InkBuffer> misc;
    
    entityx::ComponentHandle<SurfaceInk> surfInk;
    
    PingPongBuffer velDen;
    PingPongBuffer dist1;
    PingPongBuffer dist2;
    PingPongBuffer fixInk;
    PingPongBuffer flowInk;
    PingPongBuffer sinkInk;
    
    SimulationSettings settings;
    
    GetVelDenShader getVelDen;
    BlockShader block;
    GapShader gap;
    Collide1Shader collide1;
    Collide2Shader collide2;
    Stream1Shader stream1;
    Stream2Shader stream2;
    InkSupplyShader inkSupply;
    InkXAmtShader inkXAmt;
    InkXToShader inkXTo;
    InkXFrShader inkXFr;
    InkFlowShader inkFlow;
    
    float width;
    float height;
    
    ci::vec2 offset;
    
    ci::gl::FboRef inputBuffer;
    ci::gl::FboRef paperBuffer;
    
    ci::gl::TextureRef t1,t2,t3;
    
    bool calledSetup = false;
public:
    Paint();
    void update();
    void setupPaper(std::string t1, std::string t2,std::string t3);
    void begin();
    void end();
    void draw();
    void depositOnPaper();
    
    void setup(int width=ci::app::getWindowWidth(),int height=ci::app::getWindowHeight());
};
#endif /* defined(__WaterColor__Paint__) */
