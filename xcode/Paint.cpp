//
//  Paint.cpp
//  Colors
//
//  Created by Joseph Chow on 1/3/16.
//
//

#include "Paint.h"
using namespace ci;
using namespace ci::app;
using namespace std;
using namespace entityx;

Paint::Paint():entities(events),systems(entities,events){
    
    paint = entities.create();
    
    paint.assign<Watercolor>();
    paint.assign<BlendComponent>();
    paint.assign<InkBuffer>();
    paint.assign<InputBuffer>();
    paint.assign<SurfaceInk>();
    
    systems.add<InkDeposition>(&settings);
    systems.add<PaperConstruction>();
    systems.configure();
    
    //refernece to the simulation
    simulation = paint.component<Watercolor>();
    misc = paint.component<InkBuffer>();
    input = paint.component<InputBuffer>();
    surfInk = paint.component<SurfaceInk>();
}

void Paint::setup(int width, int height){
    
    this->width = width;
    this->height = height;
    
    offset = vec2(1.0) / vec2(width,height);
    
    velDen.allocate();
    dist1.allocate();
    dist2.allocate();
    flowInk.allocate();
    fixInk.allocate();
    sinkInk.allocate();

    calledSetup = true;
}


void Paint::setupPaper(std::string t1, std::string t2, std::string t3){

    paint.assign<PaperCompositor>(getWindowWidth(),getWindowHeight());
    paint.assign<PaperTextures>(t1,t2,t3);
    systems.update<PaperConstruction>(1.0);
    
    if(!calledSetup){
        setup();
    }
    
}
void Paint::begin(){
    input->begin();
}

void Paint::end(){    
    input->end();
    depositOnPaper();
}

void Paint::draw(){
    
    //gl::draw(simulation->paper);
    //gl::draw(misc->getOutput());
    //gl::draw(surfInk.getOutput());
    gl::draw(fixInk.getOutput());
}

void Paint::update(){
    misc->begin();
    block.update(width,
                 height,
                 vec2(),
                 settings.advect_p,
                 settings.b11,
                 settings.b12,
                 settings.b13,
                 settings.b21,
                 settings.b22,
                 settings.p1,
                 settings.p2,
                 settings.p3,
                 settings.toe_p,
                 settings.omega,
                 offset,
                 misc->getOutput(),
                 velDen.getOutput(),
                 flowInk.getOutput(),
                 fixInk.getOutput(),
                 simulation->paper);
    misc->end();
    
    dist1.begin();
    collide1.update(width, height,
                    vec2(),
                    settings.advect_p,
                    settings.omega,
                    velDen.getOutput(),
                    flowInk.getOutput());
    dist1.end();
    
    dist2.begin();
    collide2.update(width,
                    height,
                    vec2(),
                    settings.advect_p,
                    settings.omega,
                    velDen.getOutput(),
                    flowInk.getOutput());
    dist2.end();
    
    dist1.begin();
    stream1.update(width,
                   height,
                   vec2(),
                   settings.evapor_b,
                   offset,
                   misc->getOutput());
    dist1.end();
    
    dist2.begin();
    stream2.update(width,
                   height,
                   vec2(),
                   settings.evapor_b,
                   offset,
                   misc->getOutput(),
                   dist2.getOutput());
    dist2.end();
    
    velDen.begin();
    getVelDen.update(width,
                     height,
                     vec2(),
                     settings.wf_mul,
                     settings.evapor,
                     misc->getOutput(),
                     dist1.getOutput(),
                     dist2.getOutput());
    velDen.end();
    surfInk->begin();
    inkSupply.update(width,
                     height,
                     vec2(),
                     velDen.getOutput(),
                     misc->getOutput());
    surfInk->end();
    
    sinkInk.begin();
    inkXAmt.update(width,
                   height,
                   vec2(),
                   settings.f1,
                   settings.f2,
                   settings.f3,
                   velDen.getOutput(),
                   misc->getOutput(),
                   flowInk.getOutput(),
                   fixInk.getOutput());
    sinkInk.end();
    
    fixInk.begin();
    inkXTo.update(width,
                  height,
                  vec2(),
                  sinkInk.getOutput(),
                  velDen.getOutput(),
                  false);
    fixInk.end();
    
    flowInk.begin();
    inkXFr.update(width,
                  height,
                  vec2(),
                  sinkInk.getOutput());
    flowInk.end();
    
    flowInk.begin();
    inkFlow.update(width,
                   height,
                   vec2(),
                   settings.ba1,
                   settings.ba2,
                   offset,
                   velDen.getOutput(),
                   misc->getOutput(),
                   dist1.getOutput(),
                   dist2.getOutput(),
                   surfInk->getOutput());
    flowInk.end();
    
}


void Paint::depositOnPaper(){
    systems.update<InkDeposition>(0.0);
}
