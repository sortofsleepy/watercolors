//
//  PaperTextures.h
//  Colors
//  The various textures used to form the "paper"
//  Created by Joseph Chow on 1/5/16.
//
//

#ifndef Colors_PaperTextures_h
#define Colors_PaperTextures_h

using namespace ci;
using namespace std;
using namespace ci::app;

struct PaperTextures {
    PaperTextures ()= default;
    PaperTextures(string tex1,string tex2,string tex3){
        t1 = gl::Texture::create(loadImage(loadAsset(tex1)));
        t2 = gl::Texture::create(loadImage(loadAsset(tex2)));
        t3 = gl::Texture::create(loadImage(loadAsset(tex3)));
    }
    
    ci::gl::TextureRef t1,t2,t3;
    
};

#endif
