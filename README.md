This is a port of Akira Hayasaka's ofxInkSim to Cinder. It's more or less a direct port but slightly decoupled a bit more into a entity-component style system using [EntityX](https://github.com/alecthomas/entityx). 

### How do I get set up? ###

Currently not really suitable for use as a cinderblock/plugin quite yet but it shouldn't be too hard to fanangle into something simple. If you're just interested in running the project then 

* download the repo into `documents->projects`

If you wanna place the files elsewhere you'll have to make the appropriate changes to the xcode project file.

### Notes ###
* The `non-component-system` branch contains a port without the use of EntityX.
* Theres not really any commenting in the original ofxAddon but I've done my best to add comments to describe what is happening(or at least, what I think is happening) with each bit code. Will be studying further and updating as I go along.

### Credit ###
* [Akira Hayasaka](http://www.ampontang.com/)
* [ofxInkSim](https://github.com/Akira-Hayasaka/ofxInkSim)